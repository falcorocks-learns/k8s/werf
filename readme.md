# CICD with werf

* https://werf.io is a gitops tool to build, tag container images and to deploy kubernetes manifests (through helm templating). It can do everything with just one command: `helm converge`. `werf` uses "content based tagging" to understand if images need to be rebuilt, freeing us from having to worry about container tags ever again.
* `.helm/templates` contains the kubernetes deployment manifests, templated where appropriate
* `werfdemo` contains a minimal flask application that prints the branch, commit and url of the Gitlab Environment
* `werf.yaml` provides `werf` information about the image to be built. `werf` can help a lot in monorepos
* CI jobs for this project are [here](https://gitlab.com/falcops/ci/-/blob/main/werf.yaml)
  - `production` deploys `main` branch to https://falcops-werf.falcorocks.com
  - MRs deploy to a Gitlab Environment with a dynamic name. Here the name is `review/${CI_COMMIT_REF_SLUG}`. Gitlab Environment get a dynamic deployment URL, which in this case is `https://${CI_COMMIT_REF_SLUG}-${CI_PROJECT_PATH_SLUG}.${DEPLOYMENT_DOMAIN_NAME}` (so each MR get its own endpoint, which is continuosly redeployed with new commits to the branch). For example the currently open MR https://gitlab.com/falcops/werf/-/merge_requests/3 called `test` deploys to https://test-falcops-werf.falcorocks.com. One could also get a new deployment URL for every commit for example by setting the environment URL to `https://${CI_COMMIT_SHORT_SHA}-${CI_PROJECT_PATH_SLUG}.${DEPLOYMENT_DOMAIN_NAME}`, however you must be sure to have enough resources in your kubernetes cluster to handle this. MR Environments are automatically deleted 24h after the last commit or when the MR is closed. Environments can also be deleted manually at any point through the Gitlab UI/API. An environment [teardown job](https://gitlab.com/falcops/ci/-/blob/main/werf.yaml#L63) will be triggered by environment deletion, removing the related resources in kubernetes.

## Working with werf secrets and private image registries

Werf automatically uses Gitlab CI credentials to work with the registry, but kubernetes does not know anything about these and will fail to pull images unless a `kubernetes.io/dockerconfigjson` secret is configured.

1. generate a local werf secret key
`export WERF_SECRET_KEY=$(werf helm secret generate-secret-key)`
2. copy key value into a Gitlab CI variable `WERF_SECRET_KEY`
3. create a deploy token with `read registry` access and with username `kube-puller`
4. paste the value of the token in the field `password` of a file `.dockerconfig.json`, use the one in this repository as an example. Add this file to your `.gitignore`. It's public here as an example, this image registry is public anyways.
5. replace the value of the field `auth` in the `.dockerconfig.json` with the result of `echo -n "kube-puller:YOURTOKENHERE" | base64`
6. copy the result of `cat .dockerconfig.json | base64` in a file `.helm/secret-values.yaml`, use the one here as an example
7. encrypt the secrets file with `werf helm secret values encrypt .helm/secret-values.yaml -o .helm/secret-values.yaml`
8. commit and profit
9. if you want to edit/add secrets you can do `werf helm secret values edit .helm/secret-values.yaml`
